# Neighbourhoods prototype

Typescript React SPA with CSS modules and Redux.

Bootstraped from https://github.com/Microsoft/TypeScript-React-Starter (i.e. create-react-app), ejected scripts to include CSS modules.

Deployed to https://neighbourhoods-61fe0.firebaseapp.com/

### Get started

* `yarn install`
* `yarn start` to start Webpack dev server
* Go to `http://localhost:3000`

### Build
* `yarn build`
* Follow instructions to serve local build

### Notes
* Assuming multiple images for neighbourhoods are provided with sizes aprox. 1600x400 (HD panoramic) and 600x400 (SD mobile).

### Next steps

* ~~Optimize image load for smaller devices (i.e. create smaller images and use @2x bigger images on larger screens. Use HTML img srcset, or use image-srcset in CSS)~~
* ~~Use synthetic fonts to minimize fonts download~~
* ~~Use service worker for caching~~
* Support i18n with translations
* Revisit loading state?
* Tests

### Optimizations

#### 1. Web font optimization

Bold and Slim fonts were being downloaded whithout much benefit. Browser synthetic versions of the regular font provide the same look and feel.

https://developers.google.com/web/fundamentals/performance/optimizing-content-efficiency/webfont-optimization

![Report 1](report/1.png)

#### 2. Eliminate unnecessary downloads

When loading the desktop version we were also downloading images necessary only for the small version. Using conditional rendering prevents this while also cleaning the DOM tree of unused elements. Using [React Responsive](https://github.com/contra/react-responsive).

https://developers.google.com/web/fundamentals/performance/optimizing-content-efficiency/eliminate-downloads

![Report 2](report/2.png)

#### 3. Image optimization

The hero image was the same for both desktop and mobile version. On a mobile connection this is too slow to download and does not provide any benefit as the screen is much smaller than the natural image size. Fixed using the `<picture>` element with art direction (i.e. the mobile version is a cropped version of the bigger desktop landscape image).

https://developers.google.com/web/fundamentals/performance/optimizing-content-efficiency/image-optimization

![Report 3](report/3.png)

#### 4. Optimize critical rendering path

Even with condition rendering, images were still being fetched over the internet using `<img>` tags. SVGs can be inlined in the HTML markup and this is pretty easy in React with [SVG loader](https://github.com/jhamlet/svg-react-loader).

https://developers.google.com/web/fundamentals/performance/critical-rendering-path/optimizing-critical-rendering-path

![Report 4](report/4.png)

#### 5. Work offline

At the time, the application was not a PWA and did not work offline. Correctly registering a Service Worker using the default create-react-app SWPrecacheWebpackPlugin plugin (https://github.com/facebookincubator/create-react-app/pull/1728).

https://developers.google.com/web/fundamentals/instant-and-offline/offline-cookbook/

![Report 5](report/5.png)

#### 6. HTTP caching

The service worker webpack plugin is caching all the output from the build. Yet, some of the static assets like fonts and images were not included. Rather than caching in the service worker, I decided to have the server decide the caching mechinanism via HTTP cache headers. Fonts have max-age=1year, images have max-age=1week.

https://developers.google.com/web/fundamentals/performance/optimizing-content-efficiency/http-caching

![Report 6](report/6.png)

#### 7. HTTP/2

Not a big different given the number of critical assets, but it's given for free in Firebase :)

https://developers.google.com/web/fundamentals/performance/http2/

![Report 7](report/7.png)

#### 8. Lighthouse

Confirm PWA status with Lightouse!

![Report 8](report/8.png)