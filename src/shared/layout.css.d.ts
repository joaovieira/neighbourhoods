export const flex: string;
export const layout: string;
export const layoutHorizontal: string;
export const layoutVertical: string;
export const fullbleed: string;
