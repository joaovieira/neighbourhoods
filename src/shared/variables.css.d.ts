export const fontRegular: string;
export const fontSmall: string;
export const fontLarge: string;
export const fontXlarge: string;
export const navSmHeight: string;
export const contentWidth: string;
export const wrapperMargin: string;
