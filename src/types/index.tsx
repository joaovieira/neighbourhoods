export interface ResponsiveImage {
    default: string;
    large: string;
}

export interface Neighbourhood {
    id: string;
    name: string;
    description: string;
    restaurantCount: number;
    image: ResponsiveImage;
}

export interface Restaurant {
    id: string;
    neighbourhood: string;
    name: string;
    type: string;
    price: number;
    deliveryTime: number;   // in minutes
}

export interface NeighbourhoodState {
    neighbourhood: Neighbourhood | null;
    restaurants: Array<Restaurant>;
}