import * as React from 'react';
import * as ReactDOM from 'react-dom';
import registerServiceWorker from './registerServiceWorker';

import NeighbourhoodApp from './containers/NeighbourhoodApp';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import { neighbourhood } from './reducers/index';
import { NeighbourhoodState } from './types/index';

import './index.css';

const initialState = {
  neighbourhood: null,
  restaurants: []
};

const store = createStore<NeighbourhoodState>(
  neighbourhood,
  initialState,
  applyMiddleware(thunk)
);

ReactDOM.render(
  <Provider store={store}>
    <NeighbourhoodApp />
  </Provider>,
  document.getElementById('root') as HTMLElement
);

registerServiceWorker();