import NeighbourhoodApp from '../components/NeighbourhoodApp/NeighbourhoodApp';
import * as actions from '../actions/';
import { NeighbourhoodState } from '../types/index';
import { connect } from 'react-redux';
import { Dispatch } from 'redux';

export function mapStateToProps({ neighbourhood, restaurants }: NeighbourhoodState) {
  return { neighbourhood, restaurants };
}

export function mapDispatchToProps(dispatch: Dispatch<NeighbourhoodState>) {
  return {
    changeNeighbourhood: (name: string) => dispatch(actions.changeNeighbourhood(name))
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(NeighbourhoodApp);
