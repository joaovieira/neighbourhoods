export default [
  {
    id: '1',
    name: 'Hampstead',
    restaurantCount: 34,
    description: `Hampstead is a great place to while away a whole day. As 
    well as three of London\'s biggest and best museums, there\'re plenty 
    of other things to do that are sure to entertain and amuse everyone in 
    your group.`,
    image: {
      default: '/images/neighbourhoods/hampstead.png',
      large: '/images/neighbourhoods/hampstead-hd.png'
    }
  }
];