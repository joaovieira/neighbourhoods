import { Neighbourhood, Restaurant } from '../types';
import neighbourhoods from './neighbourhoods';
import restaurants from './restaurants';

interface RequestParams {
  page?: number;
  perPage?: number;
}

const LATENCY = 150;

export function getNeighbourhoodByName(name: string): Promise<Array<Neighbourhood>> {
  const results = neighbourhoods.filter((n: Neighbourhood) => n.name === name);

  return new Promise(resolve => {
    setTimeout(() => resolve(results), LATENCY);
  });
}

export function getRestaurants(neighbourhoodId: string, params: RequestParams = {}): Promise<Array<Restaurant>> {
  const results = restaurants.filter((r: Restaurant) => r.neighbourhood === neighbourhoodId);

  return new Promise(resolve => {
    setTimeout(() => resolve(results), LATENCY);
  });
}