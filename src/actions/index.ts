import * as constants from '../constants';
import { NeighbourhoodState, Neighbourhood, Restaurant } from '../types/index';
import { ThunkAction } from 'redux-thunk';
import * as mockServer from './mockServer';

export interface UpdateNeighbourhood {
    type: constants.UPDATE_NEIGHBOURHOOD;
    neighbourhood: Neighbourhood;
}

export interface UpdateRestaurants {
    type: constants.UPDATE_RESTAURANTS;
    restaurants: Array<Restaurant>;
}

export interface AppendRestaurants {
    type: constants.APPEND_RESTAURANTS;
    restaurants: Array<Restaurant>;
}

export type NeighbourhoodAction = UpdateNeighbourhood | UpdateRestaurants | AppendRestaurants;

export function updateNeighbourhood(neighbourhood: Neighbourhood): UpdateNeighbourhood {
    return {
        type: constants.UPDATE_NEIGHBOURHOOD,
        neighbourhood
    };
}

export function updateRestaurants(restaurants: Array<Restaurant>): UpdateRestaurants {
    return {
        type: constants.UPDATE_RESTAURANTS,
        restaurants
    };
}

export function changeNeighbourhood(name: string): ThunkAction<void, NeighbourhoodState, null> {
    return (dispatch) => {
        // startLoading();
        mockServer.getNeighbourhoodByName(name)
            .then(neighbourhood => {
                dispatch(updateNeighbourhood(neighbourhood[0]));
                return mockServer.getRestaurants(neighbourhood[0].id);
            })
            .then(restaurants => {
                dispatch(updateRestaurants(restaurants));
            })
            .then(() => {
                // stopLoading();
            });
    };
}
