import * as React from 'react';
import * as styles from './SiteNav.css';
import * as MediaQuery from 'react-responsive';
import Hamburger from './hamburger.svg';
import Logo from './logo.svg';

export interface Props { }

export interface State {
  menuOpen: boolean;
}

class SiteNav extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props);
    this.state = {
      menuOpen: false
    };
  }

  toggleMenu = () => {
    this.setState({ menuOpen: !this.state.menuOpen });
  }

  render() {
    const menuOpenClass = this.state.menuOpen ? styles.open : '';
    const navLarge = (
      <a href="/" aria-label="Home"><Logo /></a>
    );
    const navSmall = (
      <div className={styles.fixedNav}>
        <a href="/" aria-label="Home"><span>Deliveroo</span></a>
        <button className={styles.toggle} aria-label="Menu" onClick={this.toggleMenu}>
          <Hamburger className={styles.burger}/>
        </button>
      </div>
    );

    return (
      <nav role="navigation" className={[styles.SiteNav, menuOpenClass].join(' ')}>
        <MediaQuery query={styles.screenLg}>{navLarge}</MediaQuery>
        <MediaQuery query={styles.screenSm}>{navSmall}</MediaQuery>
        <ul className={styles.menu}>
          <li><a href="/">Home</a></li>
          <li><a href="/search">Search</a></li>
          <li><a href="/855">£8.55</a></li>
          <li><a href="/simon-rohrbach">Simon Rohrbach</a></li>
        </ul>
      </nav>
    );
  }
}

export default SiteNav;
