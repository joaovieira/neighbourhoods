import * as React from 'react';
import { Neighbourhood, Restaurant } from '../../types';
import Hero from '../Hero/Hero';
import RestaurantList from '../RestaurantList/RestaurantList';

export interface Props {
  neighbourhood: Neighbourhood;
  restaurants: Array<Restaurant>;
  changeNeighbourhood: (name: string) => void;
}

class NeighbourhoodApp extends React.Component<Props, {}> {
  componentDidMount () {
    this.props.changeNeighbourhood('Hampstead');
  }

  render() {
    return (
      <div>
        <Hero neighbourhood={this.props.neighbourhood} />
        <RestaurantList restaurants={this.props.restaurants} />
      </div>
    );
  }
}

export default NeighbourhoodApp;
