export const fontSmall: string;
export const fontXlarge: string;
export const introWidth: string;
export const NeighbourhoodIntro: string;
export const neighbourhoodIntro: string;
export const subtitle: string;
export const title: string;
export const description: string;
