import * as React from 'react';
import { Neighbourhood } from '../../types';
import * as styles from './NeighbourhoodIntro.css';

export interface Props {
  neighbourhood: Neighbourhood;
}

function NeighbourhoodIntro({ neighbourhood }: Props) {
  return (
    <section className={styles.NeighbourhoodIntro}>
      <div className={styles.subtitle}>
        {neighbourhood.restaurantCount} restaurants delivering to
      </div>
      <h1 className={styles.title}>{neighbourhood.name}</h1>
      <p className={styles.description}>{neighbourhood.description}</p>
    </section>
  );
}

export default NeighbourhoodIntro;
