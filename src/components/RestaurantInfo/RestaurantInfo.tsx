import * as React from 'react';
import * as MediaQuery from 'react-responsive';
import { Restaurant } from '../../types';
import * as styles from './RestaurantInfo.css';
import CaretRight from './caret-right.svg';

export interface Props {
  restaurant: Restaurant;
}

function RestaurantInfo({ restaurant }: Props) {
  const priceFormatted = Array(restaurant.price).fill('£').join('');

  return (
    <a href={`/restaurants/${restaurant.id}`} className={styles.RestaurantInfo}>
      <div className={styles.title}>{restaurant.name}</div>
      <div className={styles.info}>
        <div>{restaurant.deliveryTime} minutes</div>
        <MediaQuery query={styles.screenLg}>{priceFormatted}</MediaQuery>
        <div>{restaurant.type}</div>
      </div>
      <MediaQuery query={styles.screenSm}>
        <CaretRight className={styles.link} />
      </MediaQuery>
    </a>
  );
}

export default RestaurantInfo;
