export const screenLg: string;
export const fullbleed: string;
export const filterColor: string;
export const NeighbourhoodPicture: string;
export const neighbourhoodPicture: string;
export const picture: string;
export const filter: string;
