import * as React from 'react';
import { Neighbourhood } from '../../types';
import * as styles from './NeighbourhoodPicture.css';

export interface Props {
  neighbourhood: Neighbourhood;
}

function NeighbourhoodIntro({ neighbourhood }: Props) {
  return (
    <div className={styles.NeighbourhoodPicture}>
      {neighbourhood &&
        <picture>
          <source
            media={styles.screenLg}
            srcSet={neighbourhood.image.large || neighbourhood.image.default}
          />
          <img
            className={styles.picture}
            src={neighbourhood.image.default}
            alt={`${neighbourhood.name} picture`}
          />
        </picture>
      }
      <div className={styles.filter} />
    </div>
  );
}

export default NeighbourhoodIntro;
