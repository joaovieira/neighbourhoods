import * as React from 'react';
import { Restaurant } from '../../types';
import RestaurantInfo from '../RestaurantInfo/RestaurantInfo';
import * as styles from './RestaurantList.css';

export interface Props {
  restaurants: Array<Restaurant>;
}

function RestaurantList({ restaurants }: Props) {
  return (
    <section className={styles.RestaurantList}>
      <ul className={styles.wrapper}>
        {restaurants.map(restaurant =>
          <li key={restaurant.id}>
            <RestaurantInfo restaurant={restaurant} />
          </li>
        )}
      </ul>
    </section>
  );
}

export default RestaurantList;
