import * as React from 'react';
import { Neighbourhood } from '../../types';
import SiteNav from '../SiteNav/SiteNav';
import NeighbourhoodIntro from '../NeighbourhoodIntro/NeighbourhoodIntro';
import NeighbourhoodPicture from '../NeighbourhoodPicture/NeighbourhoodPicture';
import * as styles from './Hero.css';

export interface Props {
  neighbourhood: Neighbourhood;
}

function Hero({ neighbourhood }: Props) {
  return (
    <header className={styles.Hero}>
      <NeighbourhoodPicture neighbourhood={neighbourhood} />
      <div className={styles.wrapper}>
        <SiteNav />
        <div className={styles.intro}>
          {neighbourhood && <NeighbourhoodIntro neighbourhood={neighbourhood} />}
        </div>
      </div>
    </header>
  );
}

export default Hero;
