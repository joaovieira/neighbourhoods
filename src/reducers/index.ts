import { NeighbourhoodState } from '../types/index';
import { NeighbourhoodAction } from '../actions';
import {
  UPDATE_NEIGHBOURHOOD,
  UPDATE_RESTAURANTS,
  APPEND_RESTAURANTS
} from '../constants/index';

export function neighbourhood(
  state: NeighbourhoodState,
  action: NeighbourhoodAction
): NeighbourhoodState {
  switch (action.type) {
    case UPDATE_NEIGHBOURHOOD:
      return {
        ...state,
        neighbourhood: action.neighbourhood
      };

    case UPDATE_RESTAURANTS:
      return {
        ...state,
        restaurants: action.restaurants
      };

    case APPEND_RESTAURANTS:
      return {
        ...state,
        restaurants: state.restaurants.concat(action.restaurants)
      };

    default:
      return state;
  }
}